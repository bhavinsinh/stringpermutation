package com.bhavinc;

import java.io.File;
import java.io.IOException;

public class Main {

    public static String filepath = "/src/com/bhavinc/output.txt";

    public static void main(String[] args) {
        StringPermutation stringPermutation = new StringPermutation();
        String basePath = new File("").getAbsolutePath();
        String outFilepath = basePath + filepath;
        try {
            stringPermutation.permute("abcdef", outFilepath);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
