package com.bhavinc;

import java.io.*;
import java.util.ArrayList;

/**
 * Created by bhavin on 16-06-2016.
 */
public class StringPermutation {

    public void permute(String a, String outFilePath) throws IOException {
        if (a == null || a.isEmpty()) {
            return;
        }
        if (outFilePath != null && !outFilePath.isEmpty()) {
            BufferedWriter bufferedWriter = null;
            try {
                bufferedWriter = new BufferedWriter(new FileWriter(outFilePath));
                permute(a.toCharArray(), 0, a.length() - 1, bufferedWriter);
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (bufferedWriter != null) {
                    bufferedWriter.close();
                }
            }
        }
    }

    private void permute(char[] a, int i, int j, BufferedWriter bufferedWriter) throws IOException {
        if (i == j) {
            bufferedWriter.write(a);
            bufferedWriter.write(" ");
        } else {
            for (int k = i; k < a.length; k++) {
                char t = a[k];
                a[k] = a[i];
                a[i] = t;
                permute(a, i + 1, j, bufferedWriter);
                t = a[k];
                a[k] = a[i];
                a[i] = t;
            }
        }
    }
}
