package tests.com.bhavinc;

import com.bhavinc.StringPermutation;
import org.junit.Test;

import java.io.File;
import java.util.List;
import java.util.Scanner;

import static org.junit.Assert.*;

/**
 * Created by bhavin on 16-06-2016.
 */
public class StringPermutationTest {

    public StringPermutationTest() {
    }

    @Test
    public void permute() throws Exception {
        String basePath = new File("").getAbsolutePath();
        String filePath = basePath + "/tests/com/bhavinc/input1.txt";
        String outputFilePath = basePath + "/tests/com/bhavinc/output1.txt";
        String expectedOutputFilePath = basePath + "/tests/com/bhavinc/ExpectedOutput.txt";
        StringPermutation stringPermutation = new StringPermutation();
        Scanner sc = new Scanner(new File(filePath));
        Scanner expectedOutputScanner = new Scanner(new File(expectedOutputFilePath));
        Scanner outputScanner = new Scanner(new File(outputFilePath));
        stringPermutation.permute(sc.next(), outputFilePath);
        while (expectedOutputScanner.hasNext() && outputScanner.hasNext()) {
            assertEquals(outputScanner.next(), expectedOutputScanner.next());
        }
    }

}